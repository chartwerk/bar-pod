const baseWebpackConfig = require('./webpack.base.conf');

var conf = baseWebpackConfig;
conf.devtool = 'inline-source-map';
conf.mode = 'development';
conf.output.filename = 'index.dev.js';

module.exports = conf;
