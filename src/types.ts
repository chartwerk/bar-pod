import { Serie, Options } from '@chartwerk/core';

export type BarSerieParams = {
  matchedKey: string;
  colorFormatter: (serie: BarSerie) => string;
}
export type BarSerie = Serie & Partial<BarSerieParams>;
export type BarAdditionalOptions = {
  renderBarLabels?: boolean;
  stacked?: boolean;
  barWidth?: number; // width in x axis unit
  maxBarWidth?: number; // in px
  minBarWidth?: number; // in px
  maxAnnotationSize?: number; // in px TODO: move to annotaions
  minAnnotationSize?: number; // in px
  matching?: boolean;
  opacityFormatter?: (data: RowValues) => number;
  annotations?: {
    key: string, // matchedKey from series
     // TODO: add  enum with "triangle" option 
    color: string,
  }[];
  eventsCallbacks?: {
    contextMenu?: (data: any) => void;
  }
}
export type BarOptions = Options & Partial<BarAdditionalOptions>;
export type RowValues = {
  key: number,
  values: number[],
  additionalValues: (null | number)[], // values in datapoints third column
  colors: (string | ((data: any) => string))[],
  serieTarget: string[],
}
