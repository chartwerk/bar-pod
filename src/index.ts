import { ChartwerkPod, VueChartwerkPodMixin, TimeFormat, AxisFormat } from '@chartwerk/core';

import { BarConfig } from './models/bar_options';
import { BarSeries } from './models/bar_series';

import { BarSerie, BarOptions, RowValues } from './types';
import { findClosest } from './utils';

import * as d3 from 'd3';
import * as _ from 'lodash';


export class ChartwerkBarPod extends ChartwerkPod<BarSerie, BarOptions> {
  barYScale: null | d3.ScaleLinear<number, number> = null;
  _seriesDataForRendring = [];
  series: BarSeries;
  options: BarConfig;

  constructor(el: HTMLElement, _series: BarSerie[] = [], _options: BarOptions = {}) {
    super(el, _series, _options);
    this.series = new BarSeries(_series);
    this.options = new BarConfig(_options);
  }

  protected renderMetrics(): void {
    if(!this.series.isSeriesAvailable) {
      this.renderNoDataPointsMessage();
      return;
    }

    this.setBarPodScales();
    this.setSeriesDataForRendering();
    this.renderSerie(this._seriesDataForRendring);
  }

  get isMatchingDisabled(): boolean {
    return this.options.barOptions.matching === false || this.seriesUniqKeys.length === 0;
  }

  setSeriesDataForRendering(): void {
    if(this.isMatchingDisabled) {
      this._seriesDataForRendring = this.getZippedDataForRender(this.series.visibleSeries);
    } else {
      const matchedSeries = this.seriesForMatching.map(
        (series: BarSerie[], idx: number) => this.getZippedDataForRender(series)
      );
      this._seriesDataForRendring = this.mergeMacthedSeriesAndSort(matchedSeries);
    }
  }

  setBarPodScales(): void {
    // TODO: its a hack to avoid infinite scale function recalling
    // It should be fixed by implenting BarState
    this.barYScale = this.getYScale();
  }

  renderSerie(data: any): void {
    this.metricContainer.selectAll(`.rects-container`)
      .data(data)
      .enter().append('g')
      .attr('class', 'rects-container')
      .each((d: RowValues, rowIndex: number, nodes: any) => {
        const container = d3.select(nodes[rowIndex]);
        container.selectAll('rect')
          .data(d.values)
          .enter().append('rect')
          .style('fill', (val, idx) => this.getBarColor(d, val, idx, rowIndex))
          .attr('opacity', () => this.getBarOpacity(d))
          .attr('x', (val: number, idx: number) => {
            return this.getBarPositionX(d.key, idx);
          })
          .attr('y', (val: number, idx: number) => {
            return this.getBarPositionY(val, idx, d.values);
          })
          .attr('width', this.barWidth)
          .attr('height', (val: number) => this.getBarHeight(val))
          .on('contextmenu', this.contextMenu.bind(this))
          .on('mouseover', this.redirectEventToOverlay.bind(this))
          .on('mousemove', this.redirectEventToOverlay.bind(this))
          .on('mouseout', this.redirectEventToOverlay.bind(this))
          .on('mousedown', () => { d3.event.stopPropagation(); });

        // render bar annotations, its all hardcoded
        if(_.isEmpty(this.options.barOptions.annotations)) {
          return;
        }
        // find all series for single matchedKey
        const series = _.filter(this.series.visibleSeries, serie => _.includes(d.serieTarget, serie.target));
        const matchedKeys = _.map(series, serie => serie.matchedKey); // here matchedKeys should be equal
        const key = matchedKeys[0];

        const lastRect = _.last(container.selectAll('rect')?.nodes());
        const annotation = _.find(this.options.barOptions.annotations, a => a.key === key);
        if(!lastRect || !key || !annotation) {
          return;
        }

        const rectSelection = d3.select(lastRect);
        // render triangle
        container.append('path')
          .attr('d', () => {
            const x = Math.ceil(_.toNumber(rectSelection.attr('x')));
            const y = Math.ceil(_.toNumber(rectSelection.attr('y')));
            const options = { max: this.options.barOptions.maxAnnotationSize, min: this.options.barOptions.minAnnotationSize };
            return this.getTrianglePath(x, y, this.barWidth, options);
          })
          .attr('fill', annotation.color)
          .on('mouseover', this.redirectEventToOverlay.bind(this))
          .on('mousemove', this.redirectEventToOverlay.bind(this))
          .on('mouseout', this.redirectEventToOverlay.bind(this))
          .on('mousedown', () => { d3.event.stopPropagation(); });
      });
  }

  redirectEventToOverlay(): void {
    this.overlay?.node().dispatchEvent(new MouseEvent(d3.event.type, d3.event));
  }

  getTrianglePath(x: number, y: number, length: number, options?: { max: number, min: number }): string {
    // (x, y) - top left corner of bar
    const minTriangleSize = options?.min || 6;
    const maxTriagleSize = options?.max || 10;
    const yOffset = 4; // offset between triangle and bar
    const centerX = x + length / 2;
    const correctedLength = _.clamp(length, minTriangleSize, maxTriagleSize);

    const topY = Math.max(y - correctedLength - yOffset, 4);
    const topLeftCorner = {
      x: centerX - correctedLength / 2,
      y: topY,
    };
    const topRightCorner = {
      x: centerX + correctedLength / 2,
      y: topY,
    };
    const bottomMiddleCorner = {
      x: centerX,
      y: topY + correctedLength,
    };

    return `M ${topLeftCorner.x} ${topLeftCorner.y}
      L ${topRightCorner.x} ${topRightCorner.y}
      L ${bottomMiddleCorner.x} ${bottomMiddleCorner.y} z`;
  }

  getBarOpacity(rowValues: RowValues): number {
    if(this.options.barOptions.opacityFormatter === undefined) {
      return 1;
    }
    return this.options.barOptions.opacityFormatter(rowValues);
  }

  getBarColor(rowValues: RowValues, val: number, i: number, rowIndex: number): string {
    if(_.isFunction(rowValues.colors[i])) {
      return (rowValues.colors[i] as Function)({ rowData: rowValues, val, stackedIndex: i, rowIndex });
    }
    return (rowValues.colors[i] as string);
  }

  mergeMacthedSeriesAndSort(matchedSeries: any[]) {
    // TODO: refactor
    if(matchedSeries.length === 0) {
      throw new Error('Cant mergeMacthedSeriesAndSort');
    }
    if(matchedSeries.length === 1) {
      return matchedSeries[0];
    }
    let unionSeries = _.clone(matchedSeries[0]);
    for(let i = 1; i < matchedSeries.length; i++){
      unionSeries = [...unionSeries, ...matchedSeries[i]];
    }
    const sortedSeries = _.sortBy(unionSeries, ['key']);
    return sortedSeries;
  }

  get seriesUniqKeys(): string[] {
    if(!this.series.isSeriesAvailable) {
      return [];
    }
    const keys = this.series.visibleSeries.map(serie => serie.matchedKey);
    const uniqKeys = _.uniq(keys);
    const filteredKeys = _.filter(uniqKeys, key => key !== undefined);
    return filteredKeys;
  }

  get seriesForMatching(): BarSerie[][] {
    if(this.seriesUniqKeys.length === 0) {
      return [this.series.visibleSeries];
    }
    const seriesList = this.seriesUniqKeys.map(key => {
      const seriesWithKey = _.filter(this.series.visibleSeries, serie => serie.matchedKey === key);
      return seriesWithKey;
    });
    return seriesList;
  }

  getZippedDataForRender(series: BarSerie[]): RowValues[] {
    if(series.length === 0) {
      throw new Error('There is no visible series');
    }
    const keysColumn = _.map(series[0].datapoints, row => row[0]);
    const valuesColumns = _.map(series, serie => _.map(serie.datapoints, row => row[1]));
    // @ts-ignore
    const additionalValuesColumns = _.map(series, serie => _.map(serie.datapoints, row => row[2] !== undefined ? row[2] : null));
    const zippedAdditionalValuesColumn = _.zip(...additionalValuesColumns);
    const zippedValuesColumn = _.zip(...valuesColumns);
    const colors = _.map(series, serie => serie.colorFormatter || serie.color);
    const tagrets = _.map(series, serie => serie.target);
    const zippedData = _.zip(keysColumn, zippedValuesColumn, zippedAdditionalValuesColumn, tagrets);
    const data = _.map(zippedData, row => { return { key: row[0], values: row[1], additionalValues: row[2], colors, serieTarget: tagrets } });
    return data.filter(v => v.key !== undefined);
  }

  public renderSharedCrosshair(values: { x?: number, y?: number }): void {
    this.crosshair.style('display', null);

    const x = this.state.xScale(values.x);
    this.crosshair.select('#crosshair-line-x')
      .attr('x1', x)
      .attr('x2', x);
  }

  public hideSharedCrosshair(): void {
    this.crosshair.style('display', 'none');
  }

  onMouseMove(): void {
    // TODO: mouse move work bad with matching
    const event = d3.mouse(this.chartContainer.node());
    const eventX = event[0];
    this.crosshair.select('#crosshair-line-x')
      .attr('x1', eventX)
      .attr('x2', eventX);

    const series = this.getSeriesPointFromMousePosition(eventX);

    this.options.callbackMouseMove({
      x: d3.event.pageX,
      y: d3.event.pageY,
      time: this.state.xScale.invert(eventX),
      series,
      chartX: eventX,
      chartWidth: this.width
    });
  }

  getSeriesPointFromMousePosition(eventX: number): any[] | undefined {
    if(!this.series.isSeriesAvailable) {
      return undefined;
    }
    const mousePoisitionKey = Math.ceil(this.state.xScale.invert(eventX));
    const keys = _.map(this._seriesDataForRendring, el => el.key);
    const idx = findClosest(keys, mousePoisitionKey);

    return this._seriesDataForRendring[idx];
  }

  onMouseOver(): void {
    this.crosshair.style('display', null);
    this.crosshair.raise();
  }

  onMouseOut(): void {
    this.options.callbackMouseOut();
    this.crosshair.style('display', 'none');
  }

  contextMenu(): void {
    // maybe it is not the best name, but i took it from d3.
    d3.event.preventDefault(); // do not open browser's context menu.

    const event = d3.mouse(this.chartContainer.node());
    const eventX = event[0];
    const series = this.getSeriesPointFromMousePosition(eventX);
    this.options.callbackContextMenu({
      pageX: d3.event.pageX,
      pageY: d3.event.pageY,
      xVal: this.state.xScale.invert(eventX),
      series,
      chartX: eventX
    });
  }

  get barWidth(): number {
    // TODO: here we use first value + timeInterval as bar width. It is not a good idea
    const xAxisStartValue = _.first(this.series.visibleSeries[0].datapoints)[0];
    let width = this.state.xScale(xAxisStartValue + this.timeInterval) / 2;
    if(this.options.barOptions.barWidth !== undefined) {
      // barWidth now has axis-x dimension
      width = this.state.xScale(this.state.getMinValueX() + this.options.barOptions.barWidth);
    }
    let rectColumns = this.series.visibleSeries.length;
    if(this.options.barOptions.stacked === true) {
      rectColumns = 1;
    }
    return this.updateBarWidthWithBorders(width / rectColumns);
  }

  updateBarWidthWithBorders(width: number): number {
    let barWidth = width;
    if(this.options.barOptions.minBarWidth !== undefined) {
      barWidth = Math.max(barWidth, this.options.barOptions.minBarWidth);
    }
    if(this.options.barOptions.maxBarWidth !== undefined) {
      barWidth = Math.min(barWidth, this.options.barOptions.maxBarWidth);
    }
    return barWidth;
  }

  getBarHeight(value: number): number {
    // TODO: Property 'sign' does not exist on type 'Math'
    // @ts-ignore
    const height = Math.sign(value) * (this.barYScale(0) - this.barYScale(value));
    return height;
  }

  getBarPositionX(key: number, idx: number): number {
    let xPosition: number = this.state.xScale(key);
    if(this.options.barOptions.stacked === false) {
      xPosition += idx * this.barWidth;
    }
    return xPosition;
  }

  getBarPositionY(val: number, idx: number, values: number[]): number {
    let yPosition: number = this.barYScale(Math.max(val, 0));
    if(this.options.barOptions.stacked === true) {
      const previousBarsHeight = _.sum(
        _.map(_.range(idx), i => this.getBarHeight(values[i]))
      );
      yPosition -= previousBarsHeight;
    }
    return yPosition;
  }

  getYScale(): d3.ScaleLinear<number, number> {
    if(
      this.state.getMinValueY() === undefined ||
      this.state.getMaxValueY() === undefined
    ) {
      return d3.scaleLinear()
        .domain([1, 0])
        .range([0, this.height]);
    }
    const yMaxValue = this.getYMaxValue();
    return d3.scaleLinear()
      .domain([yMaxValue, Math.min(this.state.getMinValueY(), 0)])
      .range([0, this.height]);
  }

  getYMaxValue(): number | undefined {
    if(!this.series.isSeriesAvailable) {
      return undefined;
    }
    if(this.options.axis.y.range) {
      return _.max(this.options.axis.y.range);
    }
    let maxValue: number;
    if(this.options.barOptions.stacked === true) {
      if(this.options.barOptions.matching === true && this.seriesUniqKeys.length > 0) {
        const maxValues = this.seriesForMatching.map(series => {
          const valuesColumns = _.map(series, serie => _.map(serie.datapoints, row => row[1]));
          const zippedValuesColumn = _.zip(...valuesColumns);
          return maxValue = _.max(_.map(zippedValuesColumn, row => _.sum(row)));
        });
        return _.max(maxValues);
      } else {
        const valuesColumns = _.map(this.series.visibleSeries, serie => _.map(serie.datapoints, row => row[1]));
        const zippedValuesColumn = _.zip(...valuesColumns);
        maxValue = _.max(_.map(zippedValuesColumn, row => _.sum(row)));
      }
    } else {
      maxValue = _.max(
        this.series.visibleSeries.map(
          serie => _.maxBy<number[]>(serie.datapoints, dp => dp[1])[0]
        )
      );
    }
    return Math.max(maxValue, 0);
  }
}

// it is used with Vue.component, e.g.: Vue.component('chartwerk-bar-chart', VueChartwerkBarChartObject)
export const VueChartwerkBarChartObject = {
  // alternative to `template: '<div class="chartwerk-bar-chart" :id="id" />'`
  render(createElement) {
    return createElement(
      'div',
      {
        class: { 'chartwerk-bar-chart': true },
        attrs: { id: this.id }
      }
    )
  },
  mixins: [VueChartwerkPodMixin],
  methods: {
    render() {
      console.time('bar-render');
      const pod = new ChartwerkBarPod(document.getElementById(this.id), this.series, this.options);
      pod.render();
      console.timeEnd('bar-render');
    }
  }
};

export { BarSerie, BarOptions, TimeFormat, AxisFormat };
